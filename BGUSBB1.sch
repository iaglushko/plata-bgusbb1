EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:BGUSBB1-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Плата BGUSBB1"
Date "2017-05-09"
Rev ""
Comp ""
Comment1 ""
Comment2 "Глушко И.А."
Comment3 "Бравиков Д.Г."
Comment4 "Бравиков Д.Г."
$EndDescr
$Comp
L USB_B X2
U 1 1 5886DAB3
P 6500 2950
F 0 "X2" V 6200 2950 50  0000 C CNN
F 1 "TE Connectivity 5787834" H 6450 3150 50  0000 C CNN
F 2 "BGUSBB1:USB-B_TE_Connectivity_5787834" V 6450 2850 50  0001 C CNN
F 3 "" V 6450 2850 50  0000 C CNN
	1    6500 2950
	0    1    1    0   
$EndComp
$Comp
L TEST_1P X4
U 1 1 5886E359
P 5250 3250
F 0 "X4" V 5250 3500 50  0000 C CNN
F 1 "TEST_1P" H 5250 3450 50  0001 C CNN
F 2 "BGUSBB1:1pin" H 5450 3250 50  0001 C CNN
F 3 "" H 5450 3250 50  0000 C CNN
	1    5250 3250
	0    1    1    0   
$EndComp
$Comp
L TEST_1P X3
U 1 1 5886E448
P 5250 3150
F 0 "X3" V 5250 3400 50  0000 C CNN
F 1 "TEST_1P" H 5250 3350 50  0001 C CNN
F 2 "BGUSBB1:1pin" H 5450 3150 50  0001 C CNN
F 3 "" H 5450 3150 50  0000 C CNN
	1    5250 3150
	0    1    1    0   
$EndComp
Text Label 5950 2950 0    60   ~ 0
DP
Text Label 5950 2850 0    60   ~ 0
DM
Text Label 5950 3450 0    60   ~ 0
GZ
Wire Wire Line
	5100 2750 6200 2750
Text Label 5950 2750 0    60   ~ 0
VBUS
$Comp
L CONN_01X06 X1
U 1 1 58FE11B2
P 4900 3000
F 0 "X1" H 4900 3350 50  0000 C CNN
F 1 "Connfly Electronic DS1018-06 (FDC-06)" V 5000 3000 50  0000 C CNN
F 2 "BGUSBB1:Connfly_Electronic_DS1018-06_(FDC-06)" H 4900 3000 50  0001 C CNN
F 3 "" H 4900 3000 50  0000 C CNN
	1    4900 3000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	6600 3450 6600 3250
Text Label 5950 3050 0    60   ~ 0
GND
Wire Wire Line
	5100 2850 6200 2850
Wire Wire Line
	5100 2950 6200 2950
Wire Wire Line
	5100 3050 6200 3050
Wire Wire Line
	5100 3150 5250 3150
Wire Wire Line
	5100 3250 5250 3250
Wire Wire Line
	5200 3450 6600 3450
Wire Wire Line
	5200 3150 5200 3450
Connection ~ 5200 3250
Connection ~ 5200 3150
$EndSCHEMATC
